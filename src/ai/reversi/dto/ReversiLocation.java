package ai.reversi.dto;

import aima.core.util.datastructure.XYLocation;

/**
 * 
 * @author Vamsi Krishna J
 *
 */
public class ReversiLocation extends XYLocation {

	public enum Direction {
		NORTH, SOUTH, EAST, WEST, NORTH_EAST, NORTH_WEST, SOUTH_WEST, SOUTH_EAST;
	}

	public ReversiLocation(int x, int y) {
		super(x, y);
	}

	public ReversiLocation northEast() {
		return new ReversiLocation(getXCoOrdinate() + 1, getYCoOrdinate() - 1);
	}

	public ReversiLocation northWest() {
		return new ReversiLocation(getXCoOrdinate() - 1, getYCoOrdinate() - 1);
	}

	public ReversiLocation southWest() {
		return new ReversiLocation(getXCoOrdinate() - 1, getYCoOrdinate() + 1);
	}

	public ReversiLocation southEast() {
		return new ReversiLocation(getXCoOrdinate() + 1, getYCoOrdinate() + 1);
	}

	public ReversiLocation north() {
		XYLocation location = super.north();
		return new ReversiLocation(location.getXCoOrdinate(), location.getYCoOrdinate());
	}

	public ReversiLocation south() {
		XYLocation location = super.south();
		return new ReversiLocation(location.getXCoOrdinate(), location.getYCoOrdinate());
	}

	public ReversiLocation east() {
		XYLocation location = super.east();
		return new ReversiLocation(location.getXCoOrdinate(), location.getYCoOrdinate());
	}

	public ReversiLocation west() {
		XYLocation location = super.west();
		return new ReversiLocation(location.getXCoOrdinate(), location.getYCoOrdinate());
	}

	public ReversiLocation locationAt(Direction direction) {
		switch (direction) {
		case NORTH:
			return north();
		case SOUTH:
			return south();
		case EAST:
			return east();
		case WEST:
			return west();
		case NORTH_EAST:
			return northEast();
		case NORTH_WEST:
			return northWest();
		case SOUTH_EAST:
			return southEast();
		case SOUTH_WEST:
			return southWest();

		default:
			throw new RuntimeException("Illegal Direction");
		}
	}

	public boolean isPass() {
		return getXCoOrdinate() == -1 && getYCoOrdinate() == -1;
	}

}
