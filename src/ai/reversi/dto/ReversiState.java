package ai.reversi.dto;

import static ai.reversi.constants.GameConstants.HEIGHT;
import static ai.reversi.constants.GameConstants.WIDTH;
import static ai.reversi.constants.GameConstants.Stone.BLACK;
import static ai.reversi.constants.GameConstants.Stone.EMPTY;
import static ai.reversi.constants.GameConstants.Stone.OFFBOARD;
import static ai.reversi.constants.GameConstants.Stone.WHITE;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ai.reversi.constants.GameConstants;
import ai.reversi.constants.GameConstants.Player;
import ai.reversi.constants.GameConstants.Stone;
import ai.reversi.dto.ReversiLocation.Direction;

/**
 * Reversi State: Maintains board state.
 * 
 * @author Vamsi Krishna J
 *
 */
public class ReversiState implements Serializable, Cloneable {

	private static final long serialVersionUID = -4519741189682148691L;

	private Stone[][] board = new Stone[HEIGHT][WIDTH];

	private Player playerToMove = Player.WHITE;

	private double utility = 0d;

	/**
	 * Default constructor. Initializes board in starting position.
	 */
	public ReversiState() {
		initilaizeBoard();
	}

	/**
	 * Initializing board in starting position.
	 */
	public void initilaizeBoard() {
		for (Stone[] boardC : board) {
			Arrays.fill(boardC, GameConstants.Stone.OFFBOARD);
		}
		for (int i = 1; i < HEIGHT - 1; i++) {
			for (int j = 1; j < WIDTH - 1; j++) {
				board[i][j] = EMPTY;
			}
		}
		board[HEIGHT / 2 - 1][WIDTH / 2 - 1] = WHITE;
		board[HEIGHT / 2][WIDTH / 2 - 1] = BLACK;
		board[HEIGHT / 2 - 1][WIDTH / 2] = BLACK;
		board[HEIGHT / 2][WIDTH / 2] = WHITE;

	}

	public Stone[][] getBoard() {
		return board;
	}

	public Player getCurrentPlayer() {
		return playerToMove;
	}

	/**
	 * Checks whether the player can place the stone in that position.
	 * 
	 * @param location
	 * @param player
	 * @return {@link Boolean}
	 */
	public boolean isLegalMove(ReversiLocation location, Player player) {
		Stone stoneToPlace = player.getStone();
		// Check if that position is empty
		if (board[location.getYCoOrdinate()][location.getXCoOrdinate()] == EMPTY) {
			for (Direction direction : Direction.values()) {
				ReversiLocation newLocation = location.locationAt(direction);
				Stone currentStone = board[newLocation.getYCoOrdinate()][newLocation.getXCoOrdinate()];
				// Skip : If the position in this direction is not occupied by
				// the opposite color.
				if (currentStone == Stone.EMPTY || currentStone == stoneToPlace || currentStone == OFFBOARD) {
					continue;
				}
				do {
					newLocation = newLocation.locationAt(direction);
					currentStone = board[newLocation.getYCoOrdinate()][newLocation.getXCoOrdinate()];
					if (currentStone == stoneToPlace) {
						return true;
					} else if (currentStone == EMPTY || currentStone == OFFBOARD) {
						break;
					}
				} while (true);

			}
		}
		return false;
	}

	/**
	 * Returns list of legal moves.
	 * 
	 * @return {@link List}
	 */
	public List<ReversiLocation> getLegalActions() {
		return getLegalActions(playerToMove);
	}

	/**
	 * Returns list of legal move postions for a particular player.
	 * 
	 * @param player
	 * @return {@link List}
	 */
	public List<ReversiLocation> getLegalActions(Player player) {
		List<ReversiLocation> actions = new ArrayList<>();
		for (int i = 1; i < HEIGHT - 1; i++) {
			for (int j = 1; j < WIDTH - 1; j++) {
				ReversiLocation location = new ReversiLocation(j, i);
				if (isLegalMove(location, player)) {
					actions.add(location);
				}

			}
		}
		return actions;
	}

	/**
	 * Checks whether a player has any legal moves available.
	 * 
	 * @param player
	 * @return {@link Boolean}
	 */
	public boolean hasLegalAction(Player player) {
		return !getLegalActions().isEmpty();
	}

	/**
	 * Checks whether the board is full
	 * 
	 * @return
	 */
	public boolean isBoardFull() {
		for (int i = 1; i < HEIGHT - 1; i++) {
			for (int j = 1; j < WIDTH - 1; j++) {
				if (board[i][j] == EMPTY) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Verify the terminal state for the board.
	 * 
	 * @return
	 */
	public boolean isTerminal() {
		if (isBoardFull()) {
			return true;
		}
		for (int i = 1; i < HEIGHT - 1; i++) {
			for (int j = 1; j < WIDTH - 1; j++) {
				if (isLegalMove(new ReversiLocation(j, i), Player.WHITE)) {
					return false;
				} else if (isLegalMove(new ReversiLocation(j, i), Player.BLACK)) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Clone present state to new object
	 */
	@Override
	public ReversiState clone() {
		ReversiState copy = null;
		try {
			copy = (ReversiState) super.clone();
			copy.board = new Stone[board.length][];
			for (int i = 0; i < board.length; i++) {
				copy.board[i] = Arrays.copyOf(board[i], board[i].length);
			}
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return copy;
	}

	/**
	 * Get utility for the current player
	 * 
	 * @return
	 */
	public double getUtility() {
		return getUtility(getCurrentPlayer());
	}

	/**
	 * Get Utility for the given player.
	 * 
	 * @param player
	 * @return
	 */
	public double getUtility(Player player) {
		utility = 0;
		Player opponent = player == Player.WHITE ? Player.BLACK : Player.WHITE;
		double stones = 0d;
		for (int i = 1; i < HEIGHT - 1; i++) {
			for (int j = 1; j < WIDTH - 1; j++) {
				Stone stone = board[j][i];
				if (player.getStone() == stone) {
					utility += GameConstants.WEIGHTS[j][i];
					stones++;
				} else if (stone == opponent.getStone()){
					utility -= GameConstants.WEIGHTS[j][i];
				}
			}
		}
		// utility -= 2 * stones;
		return utility;
	}

	/**
	 * Position a stone in the given location.
	 * 
	 * @param location
	 * @param player
	 */
	public void placeStone(ReversiLocation location, Player player) {
		flipStones(location, player.getStone());
		getUtility();
		playerToMove = (playerToMove == Player.WHITE ? Player.BLACK : Player.WHITE);
	}

	/**
	 * Flip stones assuming if stone is placed in this location.
	 * 
	 * @param location
	 * @param stone
	 */
	public void flipStones(ReversiLocation location, Stone stoneToPlace) {
		// Check if that position is empty
		if (board[location.getYCoOrdinate()][location.getXCoOrdinate()] == EMPTY) {
			for (Direction direction : Direction.values()) {
				ReversiLocation newLocation = location.locationAt(direction);
				Stone currentStone = board[newLocation.getYCoOrdinate()][newLocation.getXCoOrdinate()];
				// Skip : If the position in this direction is not occupied by
				// the opposite color.
				if (currentStone == Stone.EMPTY || currentStone == stoneToPlace || currentStone == OFFBOARD) {
					continue;
				}
				do {
					newLocation = newLocation.locationAt(direction);
					currentStone = board[newLocation.getYCoOrdinate()][newLocation.getXCoOrdinate()];
					if (currentStone == stoneToPlace) {
						ReversiLocation tmpLocation = new ReversiLocation(location.getXCoOrdinate(),
								location.getYCoOrdinate());
						while (!(tmpLocation.getXCoOrdinate() == newLocation.getXCoOrdinate()
								&& tmpLocation.getYCoOrdinate() == newLocation.getYCoOrdinate())) {
							board[tmpLocation.getYCoOrdinate()][tmpLocation.getXCoOrdinate()] = stoneToPlace;
							tmpLocation = tmpLocation.locationAt(direction);
						}
						break;

					} else if (currentStone == EMPTY || currentStone == OFFBOARD) {
						break;
					}
				} while (true);

			}
		}
	}

	/**
	 * Update current player
	 * 
	 * @param playerToMove
	 */
	public void setPlayerToMove(Player playerToMove) {
		this.playerToMove = playerToMove;
	}

}
