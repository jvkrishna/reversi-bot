package ai.reversi.dto;

import java.io.Serializable;
import java.util.List;

import ai.reversi.constants.GameConstants.Player;
import aima.core.search.adversarial.Game;

/**
 * Implementation of {@link Game}.
 * 
 * @author Vamsi Krishna J
 *
 */
public class ReversiGame implements Game<ReversiState, ReversiLocation, Player>, Serializable {

	private static final long serialVersionUID = 5251627733618212642L;

	private ReversiState initialState = new ReversiState();

	/**
	 * get Initial state of the game
	 */
	@Override
	public ReversiState getInitialState() {
		return initialState;
	}

	/**
	 * get all available players.
	 */
	@Override
	public Player[] getPlayers() {
		return Player.values();
	}

	/**
	 * get current player
	 */
	@Override
	public Player getPlayer(ReversiState state) {
		return state.getCurrentPlayer();
	}

	/**
	 * get all possible location available for a given state.
	 */
	@Override
	public List<ReversiLocation> getActions(ReversiState state) {
		return state.getLegalActions();
	}

	/**
	 * get next sate if user performs a action on present state.
	 */
	@Override
	public ReversiState getResult(ReversiState state, ReversiLocation action) {
		ReversiState result = state.clone();
		result.placeStone(action, state.getCurrentPlayer());
		return result;
	}

	/**
	 * Check the terminal state
	 */
	@Override
	public boolean isTerminal(ReversiState state) {
		return state.isTerminal();
	}

	/**
	 * Get utility for a player in the given state.
	 * 
	 * @return {@link Double}
	 */
	@Override
	public double getUtility(ReversiState state, Player player) {
		double result = state.getUtility(player);
		return result;
	}

}
