package ai.reversi.constants;

/**
 * Constants class.
 * 
 * @author Vamsi Krishna J
 *
 */
public class GameConstants {

	public static final int WIDTH = 10;
	public static final int HEIGHT = 10;

	public static final int DEFAULT_DEPTH_LIMIT = 4;

	public static int[][] WEIGHTS = new int[WIDTH][HEIGHT];
	static {
		WEIGHTS[1][1] = Weights.CORNER.getValue();
		WEIGHTS[2][2] = Weights.X_SQURE.getValue();
		WEIGHTS[1][2] = WEIGHTS[2][1] = Weights.C_SQUARE.getValue();
		WEIGHTS[1][3] = WEIGHTS[3][1] = Weights.A_SQUARE.getValue();
		WEIGHTS[1][4] = WEIGHTS[4][1] = Weights.B_SQUARE.getValue();
		WEIGHTS[2][3] = WEIGHTS[3][2] = -4;
		WEIGHTS[4][2] = WEIGHTS[2][4] = -3;
		WEIGHTS[3][3] = 7;
		WEIGHTS[3][4] = WEIGHTS[4][3] = 4;

		for (int i = 5; i <= 8; i++) {
			for (int j = 1; j <= 4; j++) {
				WEIGHTS[j][i] = WEIGHTS[(j)][9 - i];
			}
		}

		for (int i = 1; i <= 8; i++) {
			for (int j = 5; j <= 8; j++) {
				WEIGHTS[j][i] = WEIGHTS[9 - j][(i)];
			}
		}

		for (int i = 0; i <= 9; i++) {
			WEIGHTS[i][0] = WEIGHTS[i][9] = WEIGHTS[0][i] = WEIGHTS[9][i] = 0;
		}
	}

	public enum Player {
		WHITE(Stone.WHITE), BLACK(Stone.BLACK);
		Stone stone;

		private Player(Stone stone) {
			this.stone = stone;
		}

		public Stone getStone() {
			return stone;
		}
	}

	public enum Stone {
		WHITE(1), BLACK(2), EMPTY(0), OFFBOARD(-1);
		int value;

		private Stone(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	public enum Weights {
		/*
		 * CORNER(60), X_SQURE(-20), C_SQUARE(-10), A_SQUARE(10), B_SQUARE(8),
		 * COMMON(3);
		 */
		CORNER(99), X_SQURE(-24), C_SQUARE(-8), A_SQUARE(8), B_SQUARE(6);
		int value;

		private Weights(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}
}
