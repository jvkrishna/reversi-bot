package ai.reversi.util;

import ai.reversi.constants.GameConstants;
import ai.reversi.constants.GameConstants.Player;
import ai.reversi.constants.GameConstants.Stone;
import aima.core.util.datastructure.XYLocation;

public class Utility {

	public static boolean isValidLocation(XYLocation location) {
		return location.getXCoOrdinate() <= GameConstants.WIDTH && location.getYCoOrdinate() <= GameConstants.HEIGHT;
	}

	public static void printBoard(Stone[][] board) {
		for (int i = 1; i < GameConstants.WIDTH - 1; i++) {
			for (int j = 1; j < GameConstants.HEIGHT - 1; j++) {
				Stone stone = board[i][j];
				if (stone == Stone.EMPTY) {
					System.out.print(stone.getValue() + "\t");
				} else {
					System.out.print(stone.name().charAt(0) + "\t");
				}
			}
			System.out.println();
			System.out.println();
		}
	}

	public static int getTotalStones(Stone[][] board, Player player) {
		int stones = 0;
		for (int i = 1; i < GameConstants.WIDTH - 1; i++) {
			for (int j = 1; j < GameConstants.HEIGHT - 1; j++) {
				Stone stone = board[i][j];
				if (stone == player.getStone()) {
					stones++;
				}
			}
		}
		return stones;
	}
}
