package ai.reversi.search;

import java.util.Comparator;

import aima.core.search.adversarial.Game;

public class ReversiScoreComparator<STATE, ACTION, PLAYER> implements Comparator<ACTION> {
	protected Game<STATE, ACTION, PLAYER> game;
	private STATE state;
	private PLAYER player;

	public ReversiScoreComparator(Game<STATE, ACTION, PLAYER> game, STATE state, PLAYER player) {
		this.game = game;
		this.state = state;
		this.player = player;

	}

	@Override
	public int compare(ACTION o1, ACTION o2) {
		Double util1 = game.getUtility(game.getResult(state, o1), player);
		Double util2 = game.getUtility(game.getResult(state, o2), player);
		return util1.compareTo(util2);
	}

}
