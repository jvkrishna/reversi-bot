package ai.reversi.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.swing.Action;

import aima.core.environment.map.Map;
import aima.core.search.adversarial.Game;
import aima.core.search.adversarial.IterativeDeepeningAlphaBetaSearch;

public class ReversiSearch<STATE, ACTION, PLAYER> extends IterativeDeepeningAlphaBetaSearch<STATE, ACTION, PLAYER> {

	private int expandedNodes;
	private int maxDepth;
	private boolean maxDepthReached = false;
	private boolean logEnabled = true;
	private long maxTime;

	public ReversiSearch(Game<STATE, ACTION, PLAYER> game, double utilMin, double utilMax, int time) {
		super(game, utilMin, utilMax, time);
		this.maxTime = time;
	}

	public void setMaxDepth(int maxDepth) {
		this.maxDepth = maxDepth;
	}

	public void setCurrDepthLimit(int curDepthLimit) {
		this.currDepthLimit = curDepthLimit;
	}

	@Override
	public ACTION makeDecision(STATE state) {
		List<ACTION> results = null;
		double resultValue = Double.NEGATIVE_INFINITY;
		PLAYER player = game.getPlayer(state);
		StringBuffer logText = null;
		expandedNodes = 0;
		maxDepth = 0;
		long startTime = System.currentTimeMillis();
		boolean exit = false;
		do {
			incrementDepthLimit();
			maxDepthReached = false;
			List<ACTION> newResults = new ArrayList<ACTION>();
			double newResultValue = Double.NEGATIVE_INFINITY;
			double secondBestValue = Double.NEGATIVE_INFINITY;
			if (logEnabled)
				logText = new StringBuffer("depth " + currDepthLimit + ": ");
			for (ACTION action : orderActions(state, game.getActions(state), player, 0)) {
				if (results != null && System.currentTimeMillis() > startTime + maxTime) {
					exit = true;
					break;
				}
				double value = minValue(game.getResult(state, action), player, Double.NEGATIVE_INFINITY,
						Double.POSITIVE_INFINITY, 1);
				if (logEnabled)
					logText.append(action + "->" + value + " ");
				if (value >= newResultValue) {
					if (value > newResultValue) {
						secondBestValue = newResultValue;
						newResultValue = value;
						newResults.clear();
					}
					newResults.add(action);
				} else if (value > secondBestValue) {
					secondBestValue = value;
				}
			}
			if (logEnabled)
				System.out.println(logText);
			if (!exit || isSignificantlyBetter(newResultValue, resultValue)) {
				results = newResults;
				resultValue = newResultValue;
			}
			if (!exit && results.size() == 1 && this.isSignificantlyBetter(resultValue, secondBestValue))
				break;
		} while (!exit && maxDepthReached && !hasSafeWinner(resultValue));
		return results.get(0);
	}

	@Override
	protected double eval(STATE state, PLAYER player) {
		if (!game.isTerminal(state)) {
			maxDepthReached = true;
		}
		return game.getUtility(state, player);
	}

	/**
	 * Move ordering
	 */
	@Override
	public List<ACTION> orderActions(STATE state, List<ACTION> actions, PLAYER player, int depth) {
		ReversiScoreComparator<STATE, ACTION, PLAYER> comparator = new ReversiScoreComparator<>(game, state, player);
		// Agent Move -- Maximize
		if (depth % 2 == 0) {
			Collections.sort(actions, Collections.reverseOrder(comparator));
		} else {
			// User move -- minimize
			Collections.sort(actions, comparator);
		}
		return actions;
	}

}
