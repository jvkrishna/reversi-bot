import java.util.Scanner;

import ai.reversi.constants.GameConstants;
import ai.reversi.constants.GameConstants.Player;
import ai.reversi.dto.ReversiGame;
import ai.reversi.dto.ReversiLocation;
import ai.reversi.dto.ReversiState;
import ai.reversi.search.ReversiSearch;
import ai.reversi.util.Utility;

/**
 * Main Game class for providing utility to the user
 * 
 * @author Vamsi Krishna J
 *
 */
public class Reversi {

	private static final boolean log = false;

	public static void main(String[] args) {
		ReversiGame game = new ReversiGame();
		ReversiState state = game.getInitialState();
		System.out.println("Initial State:");
		Utility.printBoard(state.getBoard());
		Scanner scn = new Scanner(System.in);
		int depthLimit = GameConstants.DEFAULT_DEPTH_LIMIT;
		while (true) {
			System.out.println("Enter one of the following command: PRINT PLACE EXIT HELP");
			String cmd = scn.next();
			if (cmd.equalsIgnoreCase("PRINT")) {
				Utility.printBoard(state.getBoard());
			} else if (cmd.equalsIgnoreCase("CONFIGURE")) {
				System.out.println("Enter Depth limit");
				try {
					depthLimit = scn.nextInt();
					System.out.println("Updated depth limit");
				} catch (IllegalArgumentException ex) {
					System.out.println("Illegal depth value. Skipping");
				}
			} else if (cmd.equalsIgnoreCase("PLACE")) {

				if (game.isTerminal(state)) {
					int whiteStones = Utility.getTotalStones(state.getBoard(), Player.WHITE);
					int blackStones = Utility.getTotalStones(state.getBoard(), Player.BLACK);
					if (whiteStones > blackStones) {
						System.out.println("Agent won with " + whiteStones + " stones.");
					} else if (blackStones > whiteStones) {
						System.out.println("User won with " + blackStones + " stones.");
					} else {
						System.out.println("Match tied.");
					}
					scn.close();
					return;
				}
				if (state.hasLegalAction(Player.BLACK)) {
					System.out.println("Enter Black position : ");
					int x, y;
					try {
						x = scn.nextInt();
						y = scn.nextInt();
					} catch (IllegalArgumentException e) {
						System.out.println("Please enter integer values.");
						continue;
					}
					ReversiLocation location = new ReversiLocation(x, y);
					if (!location.isPass()) {
						state.setPlayerToMove(Player.BLACK);
						if (!state.isLegalMove(location, Player.BLACK)) {
							System.out.println("Illegal Move, try again!.");
							continue;
						}
						state.placeStone(location, Player.BLACK);
					}
				} else {
					System.out.println("No legal Move for Black.");
				}
				ReversiSearch<ReversiState, ReversiLocation, Player> search = new ReversiSearch<>(game, 2, 40, 2);
				search.setLogEnabled(log);
				search.setCurrDepthLimit(depthLimit);
				try {
					ReversiLocation action = search.makeDecision(state);
					System.out.println("Agent Move:" + action);
					state.placeStone(action, Player.WHITE);
				} catch (Exception e) {
					System.out.println("No legal move for Agent: Pass");
				}
			} else if (cmd.equalsIgnoreCase("EXIT")) {
				scn.close();
				return;
			} else if (cmd.equalsIgnoreCase("HELP")) {
				printHelp();
			} else {
				System.out.println("Illegal Command. Supported Options are ");
				System.out.println();
				printHelp();
			}

		}

	}

	public static void printHelp() {
		System.out.println("PRINT: prints board");
		System.out.println("PLACE: Place stone");
		System.out.println("EXIT: Exit Game");
		System.out.println("CONFIGURE: Change depth limit");
		System.out.println("HELP: Print Help");
	}
}
